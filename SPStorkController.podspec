Pod::Spec.new do |s|
  s.name             = 'SPStorkController'
  s.version          = '1.2.7'
  s.summary          = 'SPStorkController.'
  s.description      = 'SPStorkController.'
  s.homepage         = 'https://github.com/slackhq/SPStorkController'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'slack' => 'opensource@slack.com' }
  s.source           = { :git => 'https://bitbucket.org/PremierSoft/spstorkcontroller.git', :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/slackhq'
  s.ios.deployment_target = '9.0'
  s.swift_version = '5.0'
  s.source_files = 'Source/SPStorkController/**/*.swift'
end